# Luca QR Code Badge Generator

This project generates static QR codes utilizable as key fob badges in the Luca system.
For further technical details, please refer to the documentation in the [Security Overview](https://luca-app.de/securityoverview/badge/badge_generation.html).

## Local prerequisites

### on MacOS

* Xcode Command Line Tools
* Latest cURL (MacOS internal cURL is actually not suitable)

### General

* bash
* Python 3
* conan
* pip

## Build

```bash
pip3 install -r requirements.txt
conan install . --build missing
clang++ -std=c++17 generation.cpp @conanbuildinfo.args -o generation -O3
```

## Configuration

Instead of using all the command line parameters described in the table below, it is easier to use a `config.sh` file to configure all environment variables. This file can then be `source`-ed before running the generation script. Relevant templates for the config file are provided above. See the internal confluence wiki for the development/testing tokens and secrets.

### Template for the Production Environment

```bash
export IS_LOCAL=0
export LUCA_BASE_URL="https://app.luca-app.de/api/v3"
export LUCA_REGISTRATION_BEARER="<...>"
export NEXENIO_VAULT_HOST="https://vault.somewhere.internal"
export NEXENIO_VAULT_MOUNTPOINT="<...>"
export NEXENIO_VAULT_KEYNAME="<...>"
export NEXENIO_VAULT_TOKEN="<...>"
```

### Template for the Development and Testing Stages

For non-production stages, a local private key can be used for badge attestation. Therefore, no `NEXENIO_VAULT_*` configuration is needed. Instead, please provide `LUCA_BASIC_AUTH` and the base64 encoded private value of the suitable attestation private key in `ATTESTATION_PRIVATE_KEY`.

```bash
export IS_LOCAL=0
export LUCA_BASE_URL="https://app-dev.luca-app.de/api/v3"
export LUCA_REGISTRATION_BEARER="<...>"
export ATTESTATION_PRIVATE_KEY="<...>"
export LUCA_BASIC_AUTH="<...>"
export LUCA_BADGE_VERSION=4
```

## Run

```bash
source config.sh
./generate_badges.sh
```

## Final Output

The final output is a CSV file containing the generated QR codes' serial numbers and the QR code content. The latter can be rendered into an QR code using a QR code generator of your choice. E.g. the command line tool `qrencode` does the job well enough:

```bash
echo -n '<QR code content>' | qrencode -o badge.png
```

### Configuration Variables and Command Line Arguments

| Description                                       | Flag                   | Variable                 | Default    | Required |
|---------------------------------------------------|------------------------|------------------------- |------------|----------|
| Host                                              | -h --luca-host         | LUCA_BASE_URL            |            |     *    |
| Bearer Token                                      | -t --token             | LUCA_REGISTRATION_BEARER |            |     *    |
| Count of codes to create                          | -c --count             | CODES_TO_GENERATE        |      1     |          |
| Technical Version of the Badges (supported: 4, 5) | -bv --badge-version    | LUCA_BADGE_VERSION       |      5     |          |
| Output Destination                                | -o --outputfile        | OUTFILE                  | output.csv |          |
| Intermediate QR Code CSV file 1                   | -q --qrfile            | QR_BASE_FILE             | qr.1.csv   |          |
| Intermediate QR Code CSV file 2                   | -s --attestationfile   | QR_ATTESTATION_BASE_FILE | qr.2.csv   |          |
| Disables TLS checks for the backend requests      | -l --local             | IS_LOCAL                 |      0     |          |
| Basic Auth if required                            | -a --auth              | LUCA_BASIC_AUTH          |            |    (*)   |
| Vault Base Path                                   | -vh --vault-host       | NEXENIO_VAULT_HOST       |            |     *    |
| Vault Mountpoint                                  | -vm --vault-mountpoint | NEXENIO_VAULT_MOUNTPOINT |            |     *    |
| Vault Key Name (for Badge Attestation)            | -vk --vault-keyname    | NEXENIO_VAULT_KEYNAME    |            |     *    |
| Vault Token                                       | -vt --vault-token      | NEXENIO_VAULT_TOKEN      |            |     *    |
| Attestaton Private Key (for Development Stages)   | -ak --attestation-key  | ATTESTATION_PRIVATE_KEY  |            |    (*)   |
| File containing Root CA Certificate for HTTPS     | -ca --ca-file          | TLS_CA_FILE              | nex_ca.pem |          |

## Internals

The badge generation steps described below are performed by the end-to-end shell script `./generate_badges.sh`. Luca developers will likely just need to run the `./generate_badges.sh` script.

### Raw Badge Generation

This step generates the raw cryptographic badge data. It contains the computational heavy lifting of the process and hence is done using a native binary.

### Remote Attestation

In this step the QR codes' content is signed by Luca's badge attestation key that is kept safe in neXenio's Hashicorp Vault installation. Hence, a valid `VAULT_TOKEN` is required to perform this step. Alternatively, one might provide an `ATTESTATION_PRIVATE_KEY` that is used for local attestation in development settings.

### Badge Registration

The final step registers the finalized badges with the Luca backend so that they can later be personalized and generate Check-Ins. A `LUCA_REGISTRATION_BEARER` is required for this step. Additionally, if you want to register badges on development or testing stages, you might need a `BASIC_AUTH` token.

## License

The Luca QR Code Badge Generator is Free Software (Open Source).

The code coming from the Luca App team is

```
SPDX-FileCopyrightText: 2021 culture4life GmbH <https://luca-app.de>
SPDX-License-Identifier: Apache-2.0
```

It makes use of components with compatible licenses.
Please see each individual file and
the licenses of each dependency for details:

- [C++ dependencies](./conanfile.txt), resolved via https://conan.io/center/.
- [Python depencencies](requirements.txt), resolved via https://pypi.org/.
- [LICENSES/](./LICENSES/)

(Make sure to follow all licenses, if you are distributing binaries.)
