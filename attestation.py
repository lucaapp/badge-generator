#!/usr/bin/env python3

""" This script takes a CSV-file containing raw data of generated Luca badge QR
    codes and obtains an attestation from the neXenio-internal Hashicorp Vault."""

# Copyright 2021 culture4life GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import csv
import sys
import base64
import threading
import queue
import time
from dataclasses import dataclass

from uuid import UUID

import requests

from Crypto.Hash         import SHA256
from Crypto.PublicKey    import ECC
from Crypto.Signature    import DSS
from Crypto.Util.asn1    import DerSequence
from Crypto.Util.number  import long_to_bytes
from Crypto.Math.Numbers import Integer

class AlreadyRegistered(Exception):
    pass

@dataclass
class ConnectionParameters:
    host: str
    mountpoint: str
    keyname: str
    token: str
    ca: str

print_mutex = threading.Lock()


def convert_to_ieee_1363_signature(rfc3279_signature):
    # Conversion is based on pycryptodome's parsing
    #   https://github.com/Legrandin/pycryptodome/blob/
    #           eda4f65718f9f9cfa3868f17675679ac583fa5bb/
    #           lib/Crypto/Signature/DSS.py#L155

    try:
        der_seq = DerSequence().decode(rfc3279_signature, strict=True)
    except (ValueError, IndexError):
        raise Exception("Failed to parse RFC 3279 signature DER sequence")
    if len(der_seq) != 2 or not der_seq.hasOnlyInts():
        raise ValueError("Malformed RFC 3279 signature DER sequence")
    primes = Integer(der_seq[0]), Integer(der_seq[1])

    return b"".join([long_to_bytes(x, 32) for x in primes ])


def convert_vault_signature_to_ieee_1363(vault_signature):
    try:
        vault, version, rfc3279_signature_b64 = vault_signature.split(":")
        assert vault == "vault"
        assert version == "v1"

        rfc3279_signature = base64.b64decode(rfc3279_signature_b64)
        return convert_to_ieee_1363_signature(rfc3279_signature)
    except:
        raise Exception("Failed to read vault signature wrapper")


def read_private_value(b64_private_value):
    b_private_value = base64.b64decode(b64_private_value)
    private_value   = int.from_bytes(b_private_value, "big")
    return ECC.construct(curve="P-256", d=private_value)


def get_attestation_from_vault(attestation_hash, cp: ConnectionParameters):
    retries = 0
    headers = {"X-Vault-Token": cp.token}
    url = "/".join([ cp.host, "v1", cp.mountpoint, "sign", cp.keyname, "sha2-256" ])

    # TODO: use Vault's batch API here
    #       https://www.vaultproject.io/api/secret/transit#batch_input-4
    json_payload = {
        "input": base64.b64encode(attestation_hash.digest()).decode('utf-8'),
        "prehashed": True,
        "marshaling_algorithm": "asn1"
    }

    while True:
        response = requests.post(
            url,
            json=json_payload,
            headers=headers,
            verify=cp.ca
        )

        if response.status_code == 200:
            break
        elif retries < 10:
            retries += 1
            exponential_backoff = 0.2 * 2 ** retries
            print(f"Got HTTP {response.status_code} (retrying after {exponential_backoff}s...)", file=sys.stderr)
            time.sleep(exponential_backoff)
        else:
            print("Failed too many times, aborting...", file=sys.stderr)
            sys.exit(1)

    return convert_vault_signature_to_ieee_1363(response.json()['data']['signature'])


def get_attestation_locally(attestation_hash, private_key):
    signer = DSS.new(private_key, "fips-186-3", encoding="binary")
    return signer.sign(attestation_hash)


q = queue.Queue()

def threadsafe_print(s):
    print_mutex.acquire()
    try:
        print(s)
    finally:
        print_mutex.release()


def get_attestation_worker(get_attestation):
    while True:
        # get an attestation task from the threadsafe queue
        row = q.get()

        # calculate hash to be attested
        user_id   = UUID(row["user_id"]).bytes
        guest_reference = base64.b64decode(row["encrypted_guest_reference"])
        attestation_hash = SHA256.new(user_id + guest_reference)

        # get attestation via an abstract callback
        attestation = get_attestation(attestation_hash)
        assert len(attestation) == 64 # IEEE-1363 is simply the concatenation of r and s
                                      # and should always be 64 bytes long for P-256


        # write to output file
        threadsafe_print("{},{},{},{},{},{},{},{}".format(row["entropy"],
                                                          row["user_id"],
                                                          row["tracing_seed"],
                                                          row["guest_public_key"],
                                                          row["ephemeral_public_key"],
                                                          row["encrypted_guest_reference"],
                                                          row["signature"],
                                                          base64.b64encode(attestation).decode("utf-8")))

        # mark task in the threadsafe queue as done
        q.task_done()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--vault-host",       help="Vault host name")
    parser.add_argument("--vault-mountpoint", help="Vault mount point of the Transit Engine to use")
    parser.add_argument("--vault-keyname",    help="Name of the signing key in Vault")
    parser.add_argument("--vault-token",      help="Provide an API token that has permission to attest badges")
    parser.add_argument("--attestation-key",  help="A base64 encoded P-256 big-endian private value used for badge attestation")
    parser.add_argument("--threads",          help="Number of worker threads to use", default=1, type=int)
    parser.add_argument("--ca",               help="Provide a path to a file containing the TLS root certs to use")
    parser.add_argument("badges",             help="CSV file generated by generation.cpp")

    args = parser.parse_args()

    with open(args.badges, "r") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            q.put(row)

    if args.attestation_key:
        def local_attestation(private_key):
            def go(attestation_hash):
                return get_attestation_locally(attestation_hash, private_key)
            return go

        private_key = read_private_value(args.attestation_key)
        attestation_closure = local_attestation(private_key)
    else:
        def remote_attestation(connection_options):
            def go(attestation_hash):
                return get_attestation_from_vault(attestation_hash, connection_options)
            return go

        connection_options = ConnectionParameters(
            host       = args.vault_host,
            mountpoint = args.vault_mountpoint,
            keyname    = args.vault_keyname,
            token      = args.vault_token,
            ca         = args.ca
        )

        attestation_closure = remote_attestation(connection_options)

    threadsafe_print("entropy,user_id,tracing_seed,guest_public_key,ephemeral_public_key,encrypted_guest_reference,guest_signature,attestation_signature")

    for i in range(args.threads):
        threading.Thread(
            target=get_attestation_worker,
            daemon=True,
            args=[attestation_closure]
        ).start()

    q.join()

if __name__ == "__main__":
    main()
