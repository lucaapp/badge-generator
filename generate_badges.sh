#!/bin/bash

# Copyright 2021 culture4life GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

CODES_TO_GENERATE=${CODES_TO_GENERATE:-1}
OUTFILE=${OUTFILE:-"output.csv"}
QR_BASE_FILE=${QR_BASE_FILE:-"qr.1.csv"}
QR_ATTESTATION_BASE_FILE=${QR_ATTESTATION_BASE_FILE:-"qr.2.csv"}
TLS_CA_FILE=${TLS_CA_FILE:-"nex_ca.pem"}
LUCA_BADGE_VERSION=${LUCA_BADGE_VERSION:-"5"}

IS_LOCAL=0

for arg in "$@"
do
    case $arg in
        -l|--local)
            IS_LOCAL=1
            shift
        ;;
        -h|--luca-host)
            LUCA_BASE_URL="$2"
            shift
            shift
        ;;
        -t|--token)
            LUCA_REGISTRATION_BEARER="$2"
            shift
            shift
        ;;
        -c|--count)
            CODES_TO_GENERATE="$2"
            shift
            shift
        ;;
        -o|--outputfile)
            OUTFILE="$2"
            shift
            shift
        ;;
        -q|--qrfile)
            QR_BASE_FILE="$2"
            shift
            shift
        ;;
        -s|--attestationfile)
            QR_ATTESTATION_BASE_FILE="$2"
            shift
            shift
        ;;
        -a|--auth)
            LUCA_BASIC_AUTH="$2"
            shift
            shift
        ;;
        -vh|--vault-host)
            NEXENIO_VAULT_HOST="$2"
            shift
            shift
        ;;
        -vm|--vault-mountpoint)
            NEXENIO_VAULT_MOUNTPOINT="$2"
            shift
            shift
        ;;
        -vk|--vault-keyname)
            NEXENIO_VAULT_KEYNAME="$2"
            shift
            shift
        ;;
        -vt|--vault-token)
            NEXENIO_VAULT_TOKEN="$2"
            shift
            shift
        ;;
        -ak|--attestation-key)
            ATTESTATION_PRIVATE_KEY="$2"
            shift
            shift
        ;;
        -bv|--badge-version)
            LUCA_BADGE_VERSION="$2"
            shift
            shift
        ;;
        -ca|--ca-file)
            TLS_CA_FILE="$2"
            shift
            shift
        ;;
    esac
done

if [ -z ${LUCA_REGISTRATION_BEARER+x} ]; then
    echo "Must provide the luca registration bearer token via env variable: \$LUCA_REGISTRATION_BEARER or via -t flag"
    exit 1
fi

if [ -z ${LUCA_BASE_URL+x} ]; then
    echo "Must provide the luca base url via env variable: \$LUCA_BASE_URL or via -h flag"
    exit 1
fi

if [ "$IS_LOCAL" -eq "1" ]; then
   CURL_FLAGS="-k"
   ATTESTATION_FLAG="--no-verify"
fi

if [ -z ${ATTESTATION_PRIVATE_KEY} ]; then
    if [ -z ${NEXENIO_VAULT_HOST+x} ]; then
        echo "Must provide NEXENIO_VAULT_HOST"
        exit 1
    fi

    if [ -z ${NEXENIO_VAULT_MOUNTPOINT+x} ]; then
        echo "Must provide NEXENIO_VAULT_MOUNTPOINT"
        exit 1
    fi

    if [ -z ${NEXENIO_VAULT_KEYNAME+x} ]; then
        echo "Must provide NEXENIO_VAULT_KEYNAME"
        exit 1
    fi

    if [ -z ${NEXENIO_VAULT_TOKEN+x} ]; then
        echo "Must provide NEXENIO_VAULT_TOKEN"
        exit 1
    fi
fi

if [ -z ${TLS_CA_FILE+x} ]; then
    echo "Must provide TLS_CA_FILE"
    exit 1
fi

nQRcodes="$CODES_TO_GENERATE"
outfile="$OUTFILE"
qrbasefile="$QR_BASE_FILE"
qrattestationbasefile="$QR_ATTESTATION_BASE_FILE"

echo "Running on host ${LUCA_BASE_URL}"
echo "Generating ${CODES_TO_GENERATE} badges (with version: ${LUCA_BADGE_VERSION})"
echo "Output: ${OUTFILE}"

attestation_endpoint="${LUCA_BASE_URL}/badges"
issuers_endpoint="${LUCA_BASE_URL}/keys/issuers"
pubkey_endpoint="${LUCA_BASE_URL}/keys/badge/current"

badgekey_json=$(curl -s $CURL_FLAGS -H "Authorization: Basic $LUCA_BASIC_AUTH" $pubkey_endpoint)
processors=$(getconf _NPROCESSORS_ONLN)

pubkey=$(echo   "$badgekey_json" | jq -r '.publicKey')
pubkeyid=$(echo "$badgekey_json" | jq -r '.keyId')
pubkeyCreatedAt=$(echo "$badgekey_json" | jq -r '.createdAt')

signature=$(echo "$badgekey_json" | jq -r '.signature')
issuer=$(echo "$badgekey_json" | jq -r '.issuerId')

issuer_json=$(curl -s $CURL_FLAGS -H "Authorization: Basic $LUCA_BASIC_AUTH" ${issuers_endpoint}/${issuer})

hdskp=$(echo "$issuer_json" | jq -r '.publicHDSKP')
healthdept=$(echo "$issuer_json" | jq -r '.name')

echo "badge public key (id: ${pubkeyid}) signed by '${healthdept}'"
echo "generating ${nQRcodes} on ${processors} processors for public key with ID ${pubkeyid}..."
./generation "$nQRcodes" "$processors" "$pubkey" "$pubkeyid" "$pubkeyCreatedAt" "$signature" "$hdskp" > "$qrbasefile"

if [ -z ${ATTESTATION_PRIVATE_KEY} ]; then
    echo "getting attestations for the QR codes from Vault..."
    python3 attestation.py --vault-host       "$NEXENIO_VAULT_HOST"         \
                           --vault-mountpoint "$NEXENIO_VAULT_MOUNTPOINT"   \
                           --vault-keyname    "$NEXENIO_VAULT_KEYNAME"      \
                           --vault-token      "$NEXENIO_VAULT_TOKEN"        \
                            --threads         $(( processors * 2 )) \
                           --ca               "$TLS_CA_FILE"        \
                           "$qrbasefile" > "$qrattestationbasefile"
else
    echo "using local ATTESTATION_PRIVATE_KEY to create attestations for the QR codes..."
    python3 attestation.py --attestation-key  "$ATTESTATION_PRIVATE_KEY" \
                            --threads         $(( processors * 2 ))      \
                           --ca               "$TLS_CA_FILE"             \
                           "$qrbasefile" > "$qrattestationbasefile"
fi

echo "registering new badges with Luca Server..."
# shellcheck disable=SC2068
python3 registration.py --bearer  "$LUCA_REGISTRATION_BEARER" \
                        --threads $(( processors * 2 ))       \
                        --endpoint "$attestation_endpoint"    \
                        --basic-auth "$LUCA_BASIC_AUTH"       \
                        --badge-version "$LUCA_BADGE_VERSION" \
                        $ATTESTATION_FLAG                     \
                        "$qrattestationbasefile" "$pubkeyid" >> "$outfile"
