/*
 * This file contains the implementation for the generation of luca's static
 * badges. Please refer to the luca Security Overview document for further
 * explanation:
 * https://luca-app.de/securityoverview/badge/badge_generation.html
 */

/*
 * Copyright 2021 culture4life GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cassert>
#include <iostream>
#include <mutex>
#include <stdexcept>
#include <thread>
#include <vector>

#include <argon2.h>

#include <botan/auto_rng.h>
#include <botan/base64.h>
#include <botan/ecdh.h>
#include <botan/ecdsa.h>
#include <botan/hash.h>
#include <botan/hex.h>
#include <botan/hkdf.h>
#include <botan/loadstor.h>
#include <botan/pubkey.h>
#include <botan/stream_cipher.h>

#include <fmt/format.h>

constexpr int entropy_length = 7;

// Note: This is the constant salt used for all badges. While it might look
// scary at first, the fact that it is static is not a problem in this case.
// argon2id is not used for password hashing here but for key derivation from
// random input data.
const auto salt = std::string("da3ae5ecd280924e");

// argon2id parameters
constexpr int t = 11;
constexpr int p = 1;
constexpr int m = 32768;
constexpr int l = 16;

using ByteBuffer = std::vector<uint8_t>;

std::mutex io_mutex;

namespace Botan {
ByteBuffer unlock(const ByteBuffer &v) { return v; }
} // namespace Botan

// Helper class to create a UUID from random byte data
class UniqueID {
public:
  ByteBuffer bytes;

public:
  UniqueID() = default;

  UniqueID(ByteBuffer randomBytes) {
    assert(randomBytes.size() == 16);
    bytes = toUuid(randomBytes);
  }

  UniqueID(Botan::RandomNumberGenerator &rng)
      : UniqueID(Botan::unlock(rng.random_vec(16))) {}

  std::string string() const {
    if (bytes.size() != 16) {
      return "";
    }

    const auto value = Botan::hex_encode(bytes, false);
    const char *str = value.c_str();

    // clang-format off
    return fmt::format("{}-{}-{}-{}-{}",
                       std::string_view(str +  0,  8),
                       std::string_view(str +  8,  4),
                       std::string_view(str + 12,  4),
                       std::string_view(str + 16,  4),
                       std::string_view(str + 20, 12));
    // clang-format on
  }

protected:
  ByteBuffer toUuid(ByteBuffer uuidSeed) {
    // Fixes six bits as specified in the UUID spec denoting a UUID of version 4
    // and variant 1. The form adheres to:
    //     xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx
    // Where M is a four-bit field denoting the version
    //   and N is a two-bit  field denoting the variant
    //
    // M => four most significant bits of the 7th byte are set to
    //      0x4, i.e. the entire 7th byte will have the form:
    //      0b1000xxxx
    //
    // N => two most significant bits of the 9th byte are set to
    //      0b10, i.e the entire 9th byte will have the form:
    //      0b10xxxxxx
    uuidSeed[6] = (uuidSeed[6] & 0x0F) | 0x40;
    uuidSeed[8] = (uuidSeed[8] & 0x3F) | 0x80;

    return uuidSeed;
  }
};

namespace fmt {
template <> struct formatter<ByteBuffer> : formatter<std::string> {
  template <typename FormatContext>
  auto format(ByteBuffer bb, FormatContext &ctx) {
    return formatter<std::string>::format(Botan::hex_encode(bb, false), ctx);
  }
};

template <> struct formatter<UniqueID> : formatter<std::string> {
  template <typename FormatContext>
  auto format(UniqueID uuid, FormatContext &ctx) {
    return formatter<std::string>::format(uuid.string(), ctx);
  }
};
} // namespace fmt

template <typename T>
ByteBuffer toByteBuffer(const T &stringish)
{
  return ByteBuffer(std::begin(stringish), std::end(stringish));
}

ByteBuffer toByteBuffer(const char* cstr)
{
  return toByteBuffer(std::string_view(cstr));
}

template <typename ContainerT>
std::unique_ptr<Botan::ECDSA_PrivateKey>
read_private_key(const ContainerT &buffer) {
  Botan::Null_RNG rng;

  try {
    const Botan::EC_Group group("secp256r1");
    const auto key_bits = Botan::BigInt::decode(buffer.data(), buffer.size());

    // We cannot use key values larger than the group's order. This is, however,
    // _extremely_ unlikely to happen. If it does, just abort.
    if (key_bits >= group.get_order()) {
      throw std::runtime_error("invalid key value, private key > group order");
    }

    auto key = std::make_unique<Botan::ECDSA_PrivateKey>(rng, group, key_bits);
    key->set_point_encoding(Botan::PointGFp::COMPRESSED);
    return key;
  } catch (const std::exception &ex) {
    std::cerr << "read_private_key failed: " << ex.what() << std::endl;
    std::terminate();
  }
}

template <typename ContainerT>
std::unique_ptr<Botan::ECDSA_PublicKey>
read_public_key(const ContainerT &buffer) {
  const auto secp256r1 = Botan::AlgorithmIdentifier(
      Botan::OID({1, 2, 840, 10045, 2, 1}),
      Botan::unlock(Botan::base64_decode("BggqhkjOPQMBBw==")));

  try {
    auto key = std::make_unique<Botan::ECDSA_PublicKey>(secp256r1,
                                                        Botan::unlock(buffer));
    key->set_point_encoding(Botan::PointGFp::COMPRESSED);
    return key;
  } catch (const std::exception &ex) {
    std::cerr << "read_public_key failed: " << ex.what() << std::endl;
    std::terminate();
  }
}

template <typename ContainerT>
ByteBuffer sign(const Botan::ECDSA_PrivateKey &privateKey,
                const ContainerT &message, Botan::RandomNumberGenerator &rng) {
  Botan::PK_Signer signer(privateKey, rng, "EMSA1(SHA-256)",
                          Botan::DER_SEQUENCE);
  signer.update(message.data(), message.size());
  return signer.signature(rng);
}

ByteBuffer ecdh(const Botan::ECDSA_PublicKey &pubKey,
                const Botan::ECDSA_PrivateKey &privKey,
                Botan::RandomNumberGenerator &rng) {
  Botan::EC_Group domain("secp256r1");

  Botan::ECDH_PrivateKey ecdhPrivKey(rng, domain, privKey.private_value());
  Botan::ECDH_PublicKey ecdhPubKey(domain, pubKey.public_point());

  Botan::PK_Key_Agreement agree(ecdhPrivKey, rng, "Raw");
  return Botan::unlock(
      agree.derive_key(32, ecdhPubKey.public_value()).bits_of());
}

ByteBuffer aes(const ByteBuffer &message, const ByteBuffer &key,
               const ByteBuffer &iv) {
  auto cipher = Botan::StreamCipher::create_or_throw("CTR(AES-128)");

  cipher->clear();
  cipher->set_key(key.data(), key.size());
  cipher->set_iv(iv.data(), iv.size());

  // copy, because cipher->encrypt() works in-place
  ByteBuffer output = message;
  cipher->encrypt(output);

  return output;
}

template <typename T> T concat(T buffer) { return buffer; }

template <typename T, typename... Ts>
T concat(const T &buffer, const Ts &...buffers) {
  auto result = concat(buffers...);
  result.insert(result.begin(), buffer.begin(), buffer.end());

  return result;
}

template <typename T>
T slice(const T &bb, const size_t start, const size_t length) {
  return ByteBuffer(bb.cbegin() + start, bb.cbegin() + start + length);
}

template <typename T1, typename T2> bool compare(const T1 &c1, const T2 &c2) {
  return std::equal(c1.begin(), c1.end(), c2.begin());
}

ByteBuffer hkdf(const ByteBuffer &ikm, const std::string &label,
                unsigned int length) {
  auto kdf = Botan::KDF::create_or_throw("HKDF(HMAC(SHA-256))");
  return Botan::unlock(kdf->derive_key(
      length, ikm.data(), ikm.size(), nullptr, 0,
      reinterpret_cast<const uint8_t *>(label.c_str()), label.size()));
}

ByteBuffer hmac(const ByteBuffer &data, const ByteBuffer &key) {
  auto mac = Botan::MessageAuthenticationCode::create("HMAC(SHA-256)");
  mac->set_key(key.data(), key.size());
  return Botan::unlock(mac->process(data.data(), data.size()));
}

class Badge {
public:
  static Badge generate(Botan::RandomNumberGenerator &rng,
                        const Botan::ECDSA_PublicKey &badgePublicKey) {
    Badge b;

    // generate badge seed
    rng.randomize(b.entropy.data(), b.entropy.size());
    argon2id_hash_raw(t, m, p, b.entropy.data(), b.entropy.size(), salt.c_str(),
                      salt.size(), b.seed.data(), b.seed.size());

    // derive basic key material (level one, not shared during Check-In)
    const size_t dataSecretLength = 16;
    const size_t tracingSeedLength = 16;
    const size_t keypairLength = 32;
    const size_t levelOneLength =
        dataSecretLength + tracingSeedLength + keypairLength;

    const auto levelOne = hkdf(b.seed, "badge_crypto_assets", levelOneLength);
    b.dataSecret = slice(levelOne, 0, dataSecretLength);
    b.tracingSeed = slice(levelOne, dataSecretLength, tracingSeedLength);
    const auto guestPrivateKey =
        slice(levelOne, dataSecretLength + tracingSeedLength, keypairLength);

    assert(compare(levelOne,
                   concat(b.dataSecret, b.tracingSeed, guestPrivateKey)));

    b.guestPrivateKey = read_private_key(guestPrivateKey);

    // derive Check-In key material (level two, required for Check-In)
    const size_t userIdLength = 16;
    const size_t verificationKeyLength = 16;
    const size_t tracingSecretLength = 16;
    const size_t levelTwoLength =
        userIdLength + verificationKeyLength + tracingSecretLength;

    const auto levelTwo =
        hkdf(b.tracingSeed, "badge_tracing_assets", levelTwoLength);
    const auto userIdSeed = slice(levelTwo, 0, userIdLength);
    b.badgeVerificationKey =
        slice(levelTwo, userIdLength, verificationKeyLength);
    b.tracingSecret = slice(levelTwo, userIdLength + verificationKeyLength,
                            tracingSecretLength);

    assert(compare(
        levelTwo, concat(userIdSeed, b.badgeVerificationKey, b.tracingSecret)));

    b.userID = UniqueID(userIdSeed);

    // generate encrypted guest reference
    auto ephKeypair = std::make_unique<Botan::ECDSA_PrivateKey>(
        rng, Botan::EC_Group("secp256r1"));
    b.ephPublicKey = std::make_unique<Botan::ECDSA_PublicKey>(
        Botan::EC_Group("secp256r1"), ephKeypair->public_point());
    b.ephPublicKey->set_point_encoding(
        Botan::PointGFp::Compression_Type::COMPRESSED);

    auto iv = b.ephPublicKey->public_key_bits();
    iv.resize(16);

    auto dhKey = ecdh(badgePublicKey, *ephKeypair, rng);
    auto encKey = hmac(dhKey, iv);
    encKey.resize(16);

    auto guestReference = concat(b.userID.bytes, b.dataSecret);

    b.encryptedGuestReference = aes(guestReference, encKey, iv);

    auto signedData = concat(toByteBuffer("CREATE_USER"),
                             b.userID.bytes,
                             b.encryptedGuestReference);
    b.signature = sign(*b.guestPrivateKey, signedData, rng);

    return b;
  }

  static std::string csvHeader() {
    return "entropy,user_id,tracing_seed,"
           "guest_public_key,ephemeral_public_key,encrypted_guest_reference,"
           "signature";
  }

  void print() const {
    auto lk = std::scoped_lock(io_mutex);

    std::cout << fmt::format(
                     "{},{},{},{},{},{},{}", entropy, userID, tracingSeed,
                     Botan::base64_encode(guestPrivateKey->public_key_bits()),
                     Botan::base64_encode(ephPublicKey->public_key_bits()),
                     Botan::base64_encode(encryptedGuestReference),
                     Botan::base64_encode(signature))
              << '\n';
  }

protected:
  Badge() : entropy(entropy_length), seed(l) {}

private:
  ByteBuffer entropy;
  ByteBuffer seed;
  std::unique_ptr<Botan::ECDSA_PrivateKey> guestPrivateKey;
  std::unique_ptr<Botan::ECDSA_PublicKey> ephPublicKey;
  ByteBuffer dataSecret;
  ByteBuffer dataEncryptionKey;
  UniqueID userID;
  ByteBuffer badgeVerificationKey;
  ByteBuffer tracingSecret;
  ByteBuffer tracingSeed;
  ByteBuffer encryptedGuestReference;
  ByteBuffer signature;
};

void validateBadgePkSignature(
    int keyId, int createdAt, ByteBuffer badgePk, ByteBuffer issuerSignature,
    std::unique_ptr<Botan::ECDSA_PublicKey> issuerPublicKey) {

  auto int32toLe = [](int32_t i) -> ByteBuffer {
    uint8_t result[4];
    Botan::store_le((uint32_t)i, result);
    return {result, result + 4};
  };

  const ByteBuffer msg =
      concat(int32toLe(keyId), int32toLe(createdAt), badgePk);

  issuerPublicKey->set_point_encoding(Botan::PointGFp::COMPRESSED);

  Botan::PK_Verifier ver(*issuerPublicKey, "EMSA1(SHA-256)",
                         Botan::DER_SEQUENCE);

  if (!ver.verify_message(msg, issuerSignature)) {
    std::cerr << "invalid issuer signature" << std::endl;
    std::terminate();
  }
}

int main(int argc, const char *argv[]) {
  if (argc != 8) {
    std::cerr << "usage: " << argv[0]
              << " <number of badges> <number of cores> <badge public key> <keyID> <timestamp> <signature> <issuer public key>\n"
              << "where\n"
              << "\t<number of badges> is the number of badges you wish to generate\n"
              << "\t<number of cores> is the number of cores in your system\n"
              << "\t<badge public key> is the public key to encrypt badge data for in base64\n"
              << "\t<keyID> is the id of the badge public key\n"
              << "\t<timestamp> is the unix timestamp of the creation of the badge public key\n"
              << "\t<signature> is the key issuer's signature in base64\n"
              << "\t<issuer public key> is the public key of the issuer in base64"
              << std::endl;
    return 1;
  }

  auto badges = atoi(argv[1]);
  auto cpus = atoi(argv[2]);

  auto badgePkBytes = Botan::base64_decode(argv[3]);
  auto badgePkKeyId = atoi(argv[4]);
  auto badgePkCreatedAt = atoi(argv[5]);

  auto issuerSignature = Botan::base64_decode(argv[6]);
  auto issuerPublicKey = Botan::base64_decode(argv[7]);

  validateBadgePkSignature(badgePkKeyId, badgePkCreatedAt, unlock(badgePkBytes),
                           unlock(issuerSignature),
                           read_public_key(issuerPublicKey));

  auto concurrent = std::max(cpus / p, 1);

  std::cerr << "going to use " << concurrent << " threads" << std::endl;
  std::cout << Badge::csvHeader() << std::endl;

  std::vector<std::thread> threads;

  Botan::AutoSeeded_RNG rng;

  auto badgePublicKey = read_public_key(badgePkBytes);

  for (int i = 0; i < concurrent; ++i) {
    threads.emplace_back([&] {
      for (int j = 0; j < badges / concurrent; ++j) {
        Badge::generate(rng, *badgePublicKey).print();
      }
    });
  }

  for (auto &t : threads) {
    t.join();
  }

  for (int i = 0; i < badges % concurrent; ++i) {
    Badge::generate(rng, *badgePublicKey).print();
  }

  return 0;
}
