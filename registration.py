#!/usr/bin/env python3

""" This script creates the final serial number <-> QR codes and
    registers then with the luca server. """

# Copyright 2021 culture4life GmbH
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import csv
import sys
import hashlib
import binascii
import base64
import threading
import queue
import time
from dataclasses import dataclass
from typing import Optional, Tuple

import requests
from thirdparty.z85encoding import z85encode
from thirdparty.base32crockford import b32crockford_encode

from Crypto.PublicKey import ECC

class AlreadyRegistered(Exception):
    pass

class RateLimitReached(Exception):
    pass

@dataclass
class ConnectionParameters:
    endpoint: str
    bearer_authorization: str
    http_authorization: str
    verify: bool = True

print_mutex = threading.Lock()


def sha256(data):
    return hashlib.sha256(data).digest()

def convert_to_uncompressed_pubkey(pkbase64):
    pkdata = base64.b64decode(pkbase64)

    pk = ECC.import_key(pkdata, curve_name="P-256")
    pk_uncompressed = pk.export_key(format="SEC1", compress=False)

    return base64.b64encode(pk_uncompressed).decode("utf-8")


def register_with_luca(user_id, pub_key, data, signature, connection_parameters: ConnectionParameters):

    payload = {
        "userId": user_id,
        "publicKey": convert_to_uncompressed_pubkey(pub_key),
        "data": data,
        "signature": signature,
    }

    retries = 0
    headers = {
        "badge-generator-authorization": connection_parameters.bearer_authorization,
    }

    if connection_parameters.http_authorization:
        headers["Authorization"] = f"Basic {connection_parameters.http_authorization}"

    while True:
        response = requests.post(
            connection_parameters.endpoint,
            json=payload,
            headers=headers,
            verify=connection_parameters.verify
        )

        if response.status_code == 204:
            break
        elif response.status_code == 409: # QR code was already registered in another run
            raise AlreadyRegistered("{} was registered already".format(user_id))
        elif response.status_code == 429: # rate limit
            raise RateLimitReached("you reached the rate limit of this endpoint")
        elif retries < 10:
            retries += 1
            exponential_backoff = 0.2 * 2 ** retries
            print(f"Got HTTP {response.status_code} (retrying after {exponential_backoff}s...)", file=sys.stderr)
            time.sleep(exponential_backoff)
        else:
            print("Failed too many times, aborting...", file=sys.stderr)
            sys.exit(1)


def make_qr_code(tracing_seed, badge_data, pub_key, signature, badge_key_id, badge_version):
    version = badge_version.to_bytes(length=1, byteorder='big')
    device_type = b"\x02"
    content = b"".join(
        [
            version,
            device_type,
            bytes([badge_key_id]),
            binascii.unhexlify(tracing_seed),
            base64.b64decode(badge_data),
            base64.b64decode(pub_key),
            base64.b64decode(signature),
        ]
    )

    checksum = sha256(content)[:4]
    content += checksum

    return z85encode(content).decode('utf-8')


def entropy_crockford(entropy):
    crockford = b32crockford_encode(binascii.unhexlify(entropy))[0:12]

    # Part of the last byte is used to encode what kind of TAN this is
    last = crockford[-1]
    if last == "0":
        crockford = crockford[:-1] + "2"
    elif last == "G":
        crockford = crockford[:-1] + "H"
    else:
        assert last == "0" or last == "G"

    return "-".join([crockford[:4], crockford[4:8], crockford[8:]])



q = queue.Queue()

def threadsafe_print(s):
    print_mutex.acquire()
    try:
        print(s)
    finally:
        print_mutex.release()

def generate_and_register_badges(connection_parameters: ConnectionParameters, badge_key_id, badge_version):
    while True:
        row = q.get()

        try:
            register_with_luca(
                row["user_id"],
                row["guest_public_key"],
                row["encrypted_guest_reference"],
                row["guest_signature"],
                connection_parameters
            )

            qrcode = make_qr_code(
                row["tracing_seed"],
                row["encrypted_guest_reference"],
                row["ephemeral_public_key"],
                row["attestation_signature"],
                badge_key_id,
                badge_version
            )

            serial = entropy_crockford(row["entropy"])

            threadsafe_print("{},{}".format(serial, qrcode))
        except AlreadyRegistered:
            pass

        q.task_done()


def supported_badge_versions():
    return [4, 5]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--endpoint",      help="URI to POST to for Luca remote attestation", default="https://staging.luca-app.de/api/v3/badges")
    parser.add_argument("--bearer",        help="Token to get access to --endpoint", default="")
    parser.add_argument("--threads",       help="Number of worker threads to use", default=1, type=int)
    parser.add_argument("--no-verify",     help="Disable HTTPS certificate verification (for dev environments)", action='store_true')
    parser.add_argument("--basic-auth",    help="Basic HTTP auth token to use")
    parser.add_argument("--badge-version", help="Badge Version to use (supported: " + ", ".join(map(str, supported_badge_versions())) + ")", type=int)
    parser.add_argument("badges",          help="CSV file generated by attestation.py")
    parser.add_argument("badge_key_id",    help="The badge keyID to reference in the QR codes", type=int)

    args = parser.parse_args()
    assert args.badge_key_id >= 0 and args.badge_key_id <= 255
    assert args.badge_version in supported_badge_versions()

    connection_options = ConnectionParameters(
        endpoint=args.endpoint,
        bearer_authorization=args.bearer,
        http_authorization=args.basic_auth if args.basic_auth else None,
        verify=not args.no_verify
    )

    with open(args.badges, "r") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            q.put(row)

    for i in range(args.threads):
        threading.Thread(
            target=generate_and_register_badges,
            daemon=True,
            args=[connection_options, args.badge_key_id, args.badge_version]
        ).start()

    q.join()

if __name__ == "__main__":
    main()
