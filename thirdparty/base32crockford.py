# This is the based on cpython's base64 package
# https://github.com/python/cpython/blob/master/Lib/base64.py
# from python 3.9, thus
#
# SPDX-FileCopyrightText: Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021 Python Software Foundation; All Rights Reserved
# SPDX-License-Identifier: PSF-2.0

# adapted to the crockford alphabet
# https://www.crockford.com/base32.html
# SPDX-FileCopyrightText: 2021 culture4life GmbH <https://luca-app.de>
# SPDX-License-Identifier: PSF-2.0
_b32alphabet = b'0123456789ABCDEFGHJKMNPQRSTVWXYZ'
_b32tab2 = {}
_b32rev = {}

bytes_types = (bytes, bytearray)  # Types acceptable as binary data

def _b32encode(alphabet, s):
    global _b32tab2

    # Delay the initialization of the table to not waste memory
    # if the function is never called
    if alphabet not in _b32tab2:
        b32tab = [bytes((i,)) for i in alphabet]
        _b32tab2[alphabet] = [a + b for a in b32tab for b in b32tab]
        b32tab = None

    if not isinstance(s, bytes_types):
        s = memoryview(s).tobytes()
    leftover = len(s) % 5
    # Pad the last quantum with zero bits if necessary
    if leftover:
        s = s + b'\0' * (5 - leftover)  # Don't use += !
    encoded = bytearray()
    from_bytes = int.from_bytes
    b32tab2 = _b32tab2[alphabet]
    for i in range(0, len(s), 5):
        c = from_bytes(s[i: i + 5], 'big')
        encoded += (b32tab2[c >> 30] +           # bits 1 - 10
                    b32tab2[(c >> 20) & 0x3ff] + # bits 11 - 20
                    b32tab2[(c >> 10) & 0x3ff] + # bits 21 - 30
                    b32tab2[c & 0x3ff]           # bits 31 - 40
                   )
    # Adjust for any leftover partial quanta
    if leftover == 1:
        encoded[-6:] = b'======'
    elif leftover == 2:
        encoded[-4:] = b'===='
    elif leftover == 3:
        encoded[-3:] = b'==='
    elif leftover == 4:
        encoded[-1:] = b'='

    return bytes(encoded)

# wrapper for crockford-flavor of base32
def b32crockford_encode(s):
    return _b32encode(_b32alphabet, s).decode("utf-8")
